# Loot Sheet NPC (5e)

This module adds an additional NPC sheet which can be used for loot containers such as chests.

![example](preview.gif)

### Rational

Creating a chest NPC allows the DM to place the chest on the map and set permissions to allow players to review all loot that has been dropped in a location before it is split between party members.

### FVTT Version:
- Tested with FVTT v0.3.8.

NOTE: A change with FVTT v0.3.3 has introduced two issues:
1.  Dragging items onto token versions of a sheet does not appear to be handled correctly and therefore does nothing. A work around for now is to use the actor version of the sheet (from the actor sidebar)

### Installation Instructions

To install a module, follow these instructions:

1. Start FVTT and browse to the Game Modules tab in the Configuration and Setup menu
2. Select the Install Module button and enter the following URL: https://gitlab.com/hooking/foundry-vtt---loot-sheet-npc/raw/master/module.json
3. Click Install and wait for installation to complete 

### Feedback

If you have any suggestions or feedback, please contact me on discord (hooking#0492)