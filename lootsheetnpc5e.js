
class LootSheet5eNPC extends ActorSheet5eNPC {
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   * @type {String}
   */
  get template() {
    const path = "public/modules/lootsheetnpc5e/template/";
    return path + "npc-sheet.html";
  }

  /* -------------------------------------------- */

  /**
   * Prepare permission info needed for GM section of loot NPC sheet.
   */
  getData() {
    const sheetData = super.getData();

    // Prepare GM Settings
    this._prepareGMSettings(sheetData.actor);

    // Prepare isGM attribute in sheet Data

    //console.log("game.user: ", game.user);
    if (game.user.isGM) sheetData.isGM = true;
    else sheetData.isGM = false;
    //console.log("sheetData.isGM: ", sheetData.isGM);

    // Return data for rendering
    return sheetData;
  }


  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);
    if ( !this.options.editable ) return;

    // Toggle Permissions
    html.find('.permission-proficiency').click(ev => this._onCyclePermissionProficiency(ev));

    // Split Coins
    html.find('.split-coins').click(ev => {
      this._distributeCoins(ev);
    });      

  }

  /* -------------------------------------------- */

  /**
   * Handle distribution of coins
   * @private
   */
  _distributeCoins(event) {
    event.preventDefault();

    let actorData = this.actor.data
    let owners = [];

    // Calculate owners
    for (let u in actorData.permission) {
      if (u != "default" && actorData.permission[u] == 3) {
        let player = game.users.get(u);
        let actor = game.actors.get(player.data.character);
        
        owners.push(actor);
      }
    }
    if (owners.length === 0) return;

    // Calculate split of currency
    let currencySplit = duplicate(actorData.data.currency);    
    for (let c in currencySplit) {      
      if(owners.length)
        currencySplit[c].value = Math.floor(currencySplit[c].value/owners.length);
      else 
        currencySplit[c].value = 0
    }

    // add currency to actors existing coins
    let msg = [];
    for (let u of owners) {      
      let currency = u.data.data.currency,
          newCurrency = {};

      for (let c in currency) {        
        // add msg for chat description
        if (currencySplit[c].value) {
          msg.push(` ${currencySplit[c].value} ${currencySplit[c].label} coins`)
        }

        // Add currency to permitted actor
        newCurrency[c] = {
          'type': currencySplit[c].type, 
          'label': currencySplit[c].label,
          'value': currency[c].value + currencySplit[c].value 
        }   
        u.update({'data.currency': newCurrency});  
      }

      // Remove currency from loot actor.
      let lootCurrency = this.actor.data.data.currency,
          zeroCurrency = {};
  
      for (let c in lootCurrency) {  
        zeroCurrency[c] = {
          'type': currencySplit[c].type, 
          'label': currencySplit[c].label,
          'value': 0 
        }
        this.actor.update({"data.currency": zeroCurrency});
      }
      
      // Create chat message for coins received
      if (msg.length != 0) {
        let message = `${u.data.name} receives: `;
        message += msg.join(",");
        ChatMessage.create({
          user: game.user._id,
          speaker: {actor: this.actor, alias: this.actor.name},
          content: message
        });
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle cycling permissions
   * @private
   */
  _onCyclePermissionProficiency(event) {

    event.preventDefault();
    let field = $(event.currentTarget).siblings('input[type="hidden"]');
    let level = parseFloat(field.val());
    const levels = [0, 3]; //const levels = [0, 2, 3];
    let idx = levels.indexOf(level),
        newLevel = levels[(idx === levels.length - 1) ? 0 : idx + 1];
    let actorId = field[0].name;
    
    // Read player permission on this actor and adjust to new level
    let formData = this.actor.data.permission;
    formData[actorId] = newLevel;

    // Save updated player permissions
    const lootPermissions = new PermissionControl(this.actor);
    lootPermissions._updateObject(event, formData);

    this._onSubmit(event);
  }

  /* -------------------------------------------- */

  /**
   * Organize and classify Items for Loot NPC sheets
   * @private
   */
  _prepareItems(actorData) {

    // Actions
    const features = {
      traps: { label: "Lock and Traps", items: [], type: "tool" },
      equipment: { label: "Items", items: [], type: "equipment" },
      consumable: { label: "Consumable", items: [], type: "consumable" },
      gems: { label: "Gemstones and Art Items", items: [], type: "backpack" },
    };

    // Iterate through items, allocating to containers
    for ( let i of actorData.items ) {
      i.img = i.img || DEFAULT_TOKEN;

      // Features
      if ( i.type === "tool" ) features.traps.items.push(i);
      else if ( i.type === "consumable" ) features.consumable.items.push(i);
      else if ( i.type === "backpack" ) features.gems.items.push(i);
      else if (["weapon", "equipment", "consumable", "tool"].includes(i.type)) features.equipment.items.push(i);
    }

    // Assign and return
    actorData.features = features;
  }

  /* -------------------------------------------- */

  /**
   * Get the font-awesome icon used to display the permission level.
   * @private
   */
  _getPermissionIcon(level) {
    const icons = {
      0: '<i class="far fa-circle"></i>',
      2: '<i class="fas fa-eye"></i>',
      3: '<i class="fas fa-check"></i>'
    };
    return icons[level];
  }

  /* -------------------------------------------- */

  /**
   * Get the font-awesome icon used to display the permission level.
   * @private
   */
  _getPermissionDescription(level) {
    const description = {
      0: "None (cannot access actor)",
      2: "Observer (access to actor but cannot access items)",
      3: "Owner (can access items and share coins)"
    };
    return description[level];
  }

  /* -------------------------------------------- */

  /**
   * Prepares GM settings to be rendered by the loot sheet.
   * @private
   */
  _prepareGMSettings(actorData) {

    const players = [],
          owners = [];
    let users = duplicate(game.users.source);

    for (let u of users) {

      //check if the user is a player 
      if (u.permission === 1 || u.permission === 2) {

        // get the name of the primary actor for a player
        const actor = game.actors.get(u.character);
        if ( actor ) {
          u.actor = actor.name;
        
          //if the user has some form of permission to the object update the actorData
          if (u._id in actorData.permission) {
            if (actorData.permission[u._id] === 3) {
              u.lootPermission = 3;
              owners.push(actor._id);
            }
            if (actorData.permission[u._id] === 2) u.lootPermission = 2;
            if (actorData.permission[u._id] === 1) u.lootPermission = 1;         
            if (actorData.permission[u._id] === 0) u.lootPermission = 0;  
          } else {
            u.lootPermission = 0;          
          }
          u.icon = this._getPermissionIcon(u.lootPermission);
          u.lootPermissionDescription = this._getPermissionDescription(u.lootPermission);
          players.push(u);  
        }
      }
    }

    // calculate the split of coins between all owners of the sheet.
    let currencySplit = duplicate(actorData.data.currency);    
    for (let c in currencySplit) {
      
      if(owners.length)
        currencySplit[c].value = Math.floor(currencySplit[c].value/owners.length);
      else 
        currencySplit[c].value = 0
    }

    let loot = {}
    loot.players = players;
    //loot.ownerList = owners;
    loot.ownerCount = owners.length;
    loot.currency = currencySplit;
    actorData.flags.loot = loot;
  }

}


// Register Loot NPC Sheet 
Actors.registerSheet("dnd5e", LootSheet5eNPC, {
  types: ["npc"],
  makeDefault: true
});


/**
 * Register a hook to convert any spell created on an actor with the LootSheet5eNPC sheet to a consumable scroll.
 */
Hooks.on('preCreateOwnedItem', (object, parentId, data) => {
  // Get the Actor
  const actor = game.actors.get(parentId);

  if ( !actor ) throw new Error(`Parent Actor ${parentId} not found`);

  // Check if Actor is an NPC
  if (actor.data.type === "character") return;

  // If the actor is using the LootSheet5eNPC then check in the item is a spell and if so update the name.
  if ((actor.data.flags.core||{}).sheetClass === "dnd5e.LootSheet5eNPC"){
    if (data.type === "spell") {
      data.name = "Scroll of " + data.name;
      data.type = "consumable";
      data.data.autoDestroy = {
        label: "Destroy on Empty",
        type: "Boolean",
        value: true
      }
      data.data.autoUse = {
        label: "Consume on Use",
        type: "Boolean",
        value: true
      }
      data.data.charges = {
        label: "Charges",
        max: 1,
        type: "Number",
        value: 1
      }
      data.data.consumableType = {
        label: "Consumable Type",
        type: "String",
        value: "scroll"
      }
    }
  } else return;

})

/**
 * Register a hook to change an NPC actors sheet type to LootSheet5eNPC
 */
/* Hooks.on('updateToken', (token) => {

  // Check if Actor is an NPC
  const actor = game.actors.get(token.data.actorId);
  if (actor.data.type === "character") return;

  console.log("token: ", token);
  console.log("actor: ", actor);

  if (token.data.bar1.value === 0 && token.data.bar1.attribute === "attributes.hp") {
    
    console.log("token is dead!");
    actor.data.flags.core.sheetClass = "dnd5e.LootSheet5eNPC";
  }

}) */

/**
 * A specific class for configuring permissions for a loot actor Entity type
 * @type {BaseEntitySheet}
 *
 * @param entity {Entity}               The Entity instance for which permissions are being configured.
 * @param [options] {Object}            Application options.
 */
